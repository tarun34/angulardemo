import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,ParamMap,Router} from '@angular/router';
@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.css']
})
export class DepartmentDetailComponent implements OnInit {

  constructor(private router : ActivatedRoute,private route:Router) { }
selectedId : any;
  ngOnInit() {

    this.router.paramMap.subscribe((params:ParamMap) =>
    {
      this.selectedId = parseInt(params.get('id'));
    }
    );
  }

  goPrevious(){
    let id = parseInt(this.selectedId)-1;
    this.route.navigate(['/departments',id]);
  }

goNext()
{
  let id = parseInt(this.selectedId)+1;
  this.route.navigate(['/departments',id]);
}

goBack()
{
  this.route.navigate(['../',{id:this.selectedId}],{relativeTo:this.router});
}
}
