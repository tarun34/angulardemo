import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  constructor(private router : Router,private route : ActivatedRoute) { }

  ngOnInit() {
  }
departments = [
  {"id":"1","name":"Anglar"},
  {"id":"2","name":"Java"},
  {"id":"3","name":"Net"},
  {"id":"4","name":"Jquery"},
  {"id":"5","name":"Ruby"}
];

onSelect(department){
this.router.navigate([department.id],{relativeTo:this.route});
}
}
